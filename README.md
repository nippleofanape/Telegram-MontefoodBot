# Telegram-MontefoodBot

### Food ordering bot for Telegram

![gif](order.gif)

This bot is currently being used by *Victoria Montefood*, a catering restaurant. Many clients were writing or making calls on Telegram already, so they decided to automate this process with a bot. After introducing a bot, the amount of orders done in Telegram almost **tripled** in just a month.

### Features

- All orders are being sent to an admin
- Add a new menu category or an item from config.rs
- Leave a note for an order
- Store users in the database (users.ron) with info about their total order sum, their preferred name and location for the next order
- Send a newsletter to subscribed users (for admin)
- Subscribe/unsubscribe option for clients
- Leave a feedback
- Extensive logging of every `Result<T>` or `Option<T>` allows to easily backtrack at what moment something went wrong.

### How to use

1. export your Bot token that is given to you by BotFather. Check [Teloxide](https://github.com/teloxide/teloxide#setting-up-your-environment) instruction how to set it up depending on your OS.
2. `git clone https://codeberg.org/nippleofanape/Telegram-MontefoodBot.git && cd Telegram-MontefoodBot`
3. either `RUST_LOG=trace cargo run` for an unoptimized development build, or `cargo build --release` for a release build. Run the binary in `target/release`.
4. Many string literals are still inside `main.rs` so need to modify them or export to `config.rs`.
5. To get a UserId(i64) either send a message to a bot and read it, or use [Json bot](https://t.me/ShowJsonBot)
