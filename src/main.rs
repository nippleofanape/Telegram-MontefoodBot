use std::fmt::{self, Debug};
use std::collections::HashMap;
use teloxide::{
    dispatching::{dialogue, dialogue::InMemStorage, UpdateHandler},
    prelude::*,
    types::{InlineKeyboardButton, InlineKeyboardMarkup, InputMedia, InputFile, InputMediaPhoto, MessageId},
    utils::command::BotCommands,
};

use crate::config::*;

mod config;
mod database;

type MyDialogue = Dialogue<State, InMemStorage<State>>;
type HandlerResult = Result<(), Box<dyn std::error::Error + Send + Sync>>;

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase", description = "Цей бот підтримує наступні команди")]
enum Command {
    #[command(description = "Почати дивитися меню")]
    Start,
    #[command(description = "Показати це меню")]
    Help,
    #[command(description = "Залишити відгук")]
    Feedback,
    #[command(description = "Скасувати замовлення")]
    Cancel,
    #[command(description = "Розсилка")]
    News,
}

#[derive(Clone, Default, Debug)]
enum State
{
    #[default]
    OnEnter,
    Main(BuyerInfo),
    Sub(BuyerInfo),
    Cart(BuyerInfo),
    ConfirmContactInfo(BuyerInfo),
    ReceiveFullName(BuyerInfo),
    ReceiveLocation(BuyerInfo),
    Notes(BuyerInfo),
    Final(BuyerInfo),
    Subscription(bool),
    SendNewsletter,
    SendFeedback,
}

#[derive(Clone, Default, Debug)]
struct BuyerInfo
{
    order: HashMap<String, u32>,
    total_sum: f32,
    note: String,
}

// Needed for creating an inline keyboard
enum MenuMarkup
{
    Main,
    Submenu(MenuKind),
    Cart,
    Finalize,
    Subscription(bool),
}

#[tokio::main]
async fn main()
{
    pretty_env_logger::init();

    let bot = Bot::from_env();

    Dispatcher::builder(bot, schema())
        .dependencies(dptree::deps![InMemStorage::<State>::new()])
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;
}

fn schema() -> UpdateHandler<Box<dyn std::error::Error + Send + Sync + 'static>>
{
    use dptree::case;

    // If inside a food menu (after pressing /start) only accept /help and /cancel commands
    let command_handler = teloxide::filter_command::<Command, _>()
        .branch(
            case![State::OnEnter]
                .branch(case![Command::Start].endpoint(start))
                .branch(case![Command::Feedback].endpoint(feedback))
                .branch(case![Command::News].endpoint(newsletter)),
        )
        .branch(case![Command::Help].endpoint(help))
        .branch(case![Command::Cancel].endpoint(cancel));

    let message_handler = Update::filter_message()
        .branch(command_handler)
        .branch(case![State::ReceiveFullName(buyer)].endpoint(receive_full_name))
        .branch(case![State::ReceiveLocation(buyer)].endpoint(receive_address))
        .branch(case![State::Notes(buyer)].endpoint(receive_notes))
        .branch(case![State::SendNewsletter].endpoint(send_newsletter))
        .branch(case![State::SendFeedback].endpoint(send_feedback))
        .branch(dptree::endpoint(invalid_state));

    let callback_query_handler = Update::filter_callback_query()
        .branch(case![State::Main(buyer)].endpoint(main_menu))
        .branch(case![State::Sub(buyer)].endpoint(sub_menu))
        .branch(case![State::Cart(buyer)].endpoint(cart_menu))
        .branch(case![State::ConfirmContactInfo(buyer)].endpoint(review_contact_info))
        .branch(case![State::Final(buyer)].endpoint(finalize_menu))
        .branch(case![State::Subscription(is_subscribed)].endpoint(subscription_menu));

    dialogue::enter::<Update, InMemStorage<State>, State, _>()
        .branch(message_handler)
        .branch(callback_query_handler)
}

//
// Menu state logic
//

async fn main_menu(
    bot: Bot,
    dialogue: MyDialogue,
    q: CallbackQuery,
    buyer: BuyerInfo,
) -> HandlerResult
{
    if let Some(button) = &q.data
    {
        let keyboard: InlineKeyboardMarkup;
        let text: String;
        let pic: &str;
        let next_state: State;

        match button.as_str()
        {
            CART_BUTTON =>
            {
                // Dont change state and only display a Back button if cart is empty
                if buyer.order.is_empty()
                {
                    // Singular button
                    let mut temp_kb: Vec<Vec<InlineKeyboardButton>> = vec![];
                    let back_button = [InlineKeyboardButton::callback(BACK_BUTTON, BACK_BUTTON)].to_vec();
                    temp_kb.push(back_button);

                    keyboard = InlineKeyboardMarkup::new(temp_kb);
                    pic = LOGO_PHOTO;
                    text = String::from(EMPTY_CART_TEXT);
                    next_state = State::Sub(buyer);
                }
                else
                {
                    keyboard = create_menu_keyboard(MenuMarkup::Cart);
                    pic = CART_PHOTO;
                    text = format!("Ваше замовлення:\n{:#?}\nСума: {}€", buyer.order, buyer.total_sum);
                    next_state = State::Cart(buyer);
                }
            }
            // Any of the submenus
            _ =>
            {
                let submenu = MENU.iter().find(|x| x.name == button).unwrap_or(&MENU[0]);

                keyboard = create_menu_keyboard(MenuMarkup::Submenu(submenu.kind));
                pic = submenu.pic;
                text = submenu.description.to_string();
                next_state = State::Sub(buyer);
            }
        }

        let pic = InputFile::file(format!("{}/{}", env!("CARGO_MANIFEST_DIR"), pic));
        let pic = InputMediaPhoto::new(pic).caption(text);
        let media = InputMedia::Photo(pic);

        bot.edit_message_media(dialogue.chat_id(), q.message.unwrap().id, media).reply_markup(keyboard).await?;
        dialogue.update(next_state).await?;
    }
    Ok(())
}

async fn sub_menu(
    bot: Bot,
    dialogue: MyDialogue,
    q: CallbackQuery,
    mut buyer: BuyerInfo,
) -> HandlerResult
{
    if let Some(inline_button) = &q.data
    {
        match inline_button.as_str()
        {
            BACK_BUTTON => switch_to_main(bot, dialogue, q.message.unwrap().id, buyer).await?,
            _ =>
            {
                let count = buyer.order.entry(inline_button.to_owned()).or_insert(0);
                *count += 1;
                // Use rsplit_once() to split off the end of the string based on some criterion
                let price = inline_button.rsplit_once(' ').map_or("suck my balls", |(_head, tail)| tail).trim_matches('€').parse::<f32>().unwrap_or(0.0);
                buyer.total_sum += price;

                bot.send_message(dialogue.chat_id(), format!("{} замовленно. {} загалом. Щоб відправти замовлення - перейдіть до кошику.", inline_button, count)).await?;
                dialogue.update(State::Sub(buyer)).await?;
            },
        }
    }
    Ok(())
}

async fn cart_menu(
    bot: Bot,
    dialogue: MyDialogue,
    q: CallbackQuery,
    buyer: BuyerInfo,
) -> HandlerResult
{
    if let Some(inline_button) = &q.data
    {
        match inline_button.as_str()
        {
            ORDER_BUTTON =>
            {
                // If client has ordered before and saved his name and location already - prompt to
                // use those or change them
                if let Some((loc, name)) = database::RegisteredUsers::has_delivery_details(dialogue.chat_id().0)
                {
                    // Inline keyboard with 2 buttons: Keep info and change it
                    let mut temp_kb: Vec<Vec<InlineKeyboardButton>> = vec![];
                    let button = [InlineKeyboardButton::callback(KEEP_PREVIOUS_INFO, KEEP_PREVIOUS_INFO)].to_vec();
                    temp_kb.push(button);
                    let button = [InlineKeyboardButton::callback(CHANGE_PREVIOUS_INFO, CHANGE_PREVIOUS_INFO)].to_vec();
                    temp_kb.push(button);
                    let keyboard = InlineKeyboardMarkup::new(temp_kb);

                    bot.send_location(dialogue.chat_id(), loc.latitude, loc.longitude).await?;
                    bot.send_message(dialogue.chat_id(), format!("Надіслати на цю локацію та це ім'я {}?", name)).reply_markup(keyboard).await?;

                    dialogue.update(State::ConfirmContactInfo(buyer)).await?;
                }
                else
                {
                    bot.send_message(dialogue.chat_id(), "Введіть, будь ласка, ваше ім'я:").await?;
                    dialogue.update(State::ReceiveFullName(buyer)).await?;
                }
            }
            // Back button
            _ => switch_to_main(bot, dialogue, q.message.unwrap().id, buyer).await?,
        }
    }
    Ok(())
}

async fn receive_full_name(
    bot: Bot,
    dialogue: MyDialogue,
    msg: Message,
    buyer: BuyerInfo,
) -> HandlerResult
{
    match msg.text().map(ToOwned::to_owned)
    {
        Some(full_name) =>
        {
            if let Ok(_) = database::RegisteredUsers::add_modify_user(dialogue.chat_id().0, database::ModifyUser::Name(full_name))
            {
                bot.send_message(dialogue.chat_id(), "Пожалуйста, маячок на карте, куда нужно доставть заказ.").await?;
                dialogue.update(State::ReceiveLocation(buyer)).await?;
            }
        }
        None =>
        {
            bot.send_message(msg.chat.id, "Пожалуйста, используйте только текст.").disable_notification(true).await?;
        }
    }
    Ok(())
}

async fn receive_address(
    bot: Bot,
    dialogue: MyDialogue,
    msg: Message,
    buyer: BuyerInfo,
) -> HandlerResult
{
    if let Some(loc) = msg.location()
    {
        if let Ok(_) = database::RegisteredUsers::add_modify_user(dialogue.chat_id().0, database::ModifyUser::Location(*loc))
        {
            finalize_markup(bot, dialogue, buyer).await?;
        }
    }
    else
    {
        bot.send_message(msg.chat.id, "Пожалуйста отправьте маячок во вложении.").disable_notification(true).await?;
    }
    Ok(())
}

async fn review_contact_info(
    bot: Bot,
    dialogue: MyDialogue,
    q: CallbackQuery,
    buyer: BuyerInfo,
) -> HandlerResult
{
    if let Some(button) = &q.data
    {
        match button.as_str()
        {
            CHANGE_PREVIOUS_INFO =>
            {
                bot.send_message(dialogue.chat_id(), "Введіть, будь ласка, ваше ім'я:").await?;
                dialogue.update(State::ReceiveFullName(buyer)).await?;
            }
            // Use info from the previous time
            _ =>
            {
                finalize_markup(bot, dialogue, buyer).await?;
            }
        }
    }
    Ok(())
}

async fn receive_notes(
    bot: Bot,
    dialogue: MyDialogue,
    msg: Message,
    mut buyer: BuyerInfo,
) -> HandlerResult
{
    match msg.text().map(ToOwned::to_owned)
    {
        Some(note) =>
        {
            buyer.note = note;
            finalize_markup(bot, dialogue, buyer).await?;
        }
        None =>
        {
            bot.send_message(msg.chat.id, "Пожалуйста укажите вашу заметку.").disable_notification(true).await?;
        }
    }
    Ok(())
}

async fn finalize_menu(
    bot: Bot,
    dialogue: MyDialogue,
    q: CallbackQuery,
    buyer: BuyerInfo,
) -> HandlerResult
{
    if let Some(button) = &q.data
    {
        match button.as_str()
        {
            BACK_BUTTON => switch_to_main(bot, dialogue, q.message.unwrap().id, buyer).await?,
            SEND_BUTTON =>
            {
                if let Err(e) = database::RegisteredUsers::add_modify_user(dialogue.chat_id().0, database::ModifyUser::TotalOrders(buyer.total_sum))
                {
                    log::error!("Failed adding {} to user's total order amount in a file 'users.ron'. {:?}", buyer.total_sum, e)
                }

                // Message to client
                bot.send_message(dialogue.chat_id(), format!("Спасибо. Ваш заказ\n {:#?}\nМьі свяжемся с вами как можно бьістрее.", buyer)).disable_notification(true).await?;

                // Message to admin
                if let Some((loc, name)) = database::RegisteredUsers::has_delivery_details(dialogue.chat_id().0)
                {
                    bot.send_message(UserId(ADMIN_ID), format!("===========================\n{}", q.from.tme_url().unwrap())).await?;
                    bot.send_message(UserId(ADMIN_ID), format!("Имя: {}\nЗаказ:\n{:#?}\nСумма: {}€\nЛокация 👇", name, buyer.order, buyer.total_sum)).await?;
                    bot.send_location(UserId(ADMIN_ID), loc.latitude, loc.longitude).await?;
                }
                dialogue.exit().await?;
            }
            NOTE_BUTTON =>
            {
                bot.send_message(dialogue.chat_id(), "Ваша нотатка:").await?;
                dialogue.update(State::Notes(buyer)).await?;
            }
            _ => log::warn!("something went wron when pressing a button in cart."),
        }
    }
    Ok(())
}

//
// Command handlers
//

async fn start(bot: Bot, dialogue: MyDialogue) -> HandlerResult {
    let buyer = BuyerInfo::default();
    let keyboard = create_menu_keyboard(MenuMarkup::Main);
    let media = InputFile::file(format!("{}/{}", env!("CARGO_MANIFEST_DIR"), LOGO_PHOTO));

    if let Err(e) = database::RegisteredUsers::add_modify_user(dialogue.chat_id().0, database::ModifyUser::Nothing)
    {
        log::error!("Failed writing to a file 'users.ron'. {:?}", e)
    }

    bot.send_photo(dialogue.chat_id(), media).caption(MENU_SEPARATOR).reply_markup(keyboard).await?;
    dialogue.update(State::Main(buyer)).await?;

    Ok(())
}

async fn help(bot: Bot, msg: Message) -> HandlerResult {
    bot.send_message(msg.chat.id, Command::descriptions().to_string()).await?;
    Ok(())
}

async fn feedback(bot: Bot, dialogue: MyDialogue) -> HandlerResult {
    bot.send_message(dialogue.chat_id(), "Напишіть ваш відгук:").await?;
    dialogue.update(State::SendFeedback).await?;
    Ok(())
}

async fn send_feedback(
    bot: Bot,
    dialogue: MyDialogue,
    msg: Message,
) -> HandlerResult
{
    match msg.text().map(ToOwned::to_owned)
    {
        Some(feedback) =>
        {
            bot.send_message(msg.chat.id, "Отзьів оставлен.").disable_notification(true).await?;
            if let Some ((_, name)) = database::RegisteredUsers::has_delivery_details(dialogue.chat_id().0)
            {
                bot.send_message(UserId(ADMIN_ID), format!("===========================\n{} оставил отзьів.\n{}", name, feedback)).await?;
            }
            else
            {
                bot.send_message(UserId(ADMIN_ID), format!("===========================\nНовьій отзьів.\n{}", feedback)).await?;
            }
            dialogue.exit().await?;
        }
        None =>
        {
            bot.send_message(msg.chat.id, "Пожалуйста введите только текст.").disable_notification(true).await?;
        }
    }
    Ok(())
}

async fn newsletter(bot: Bot, dialogue: MyDialogue) -> HandlerResult {
    if dialogue.chat_id().0 == ADMIN_ID as i64
    {
        bot.send_message(dialogue.chat_id(), "Повідомлення для розсилки.\n/cancel вийти назад.").await?;
        dialogue.update(State::SendNewsletter).await?;
    }
    else
    {
        let is_subscribed = database::RegisteredUsers::check_subscription(dialogue.chat_id().0);
        let text = if is_subscribed {"Subd"} else { "Unsubed" };
        let keyboard = create_menu_keyboard(MenuMarkup::Subscription(is_subscribed));

        bot.send_message(dialogue.chat_id(), format!("{}\nCurrently you are {}", NEWSLETTER, text)).reply_markup(keyboard).await?;
        dialogue.update(State::Subscription(is_subscribed)).await?;
    }
    Ok(())
}

async fn send_newsletter(
    bot: Bot,
    dialogue: MyDialogue,
    msg: Message,
) -> HandlerResult
{
    for id in database::RegisteredUsers::get_subscribers()
    {
        bot.forward_message(ChatId(id), dialogue.chat_id(), msg.id).await?;
    }
    bot.send_message(dialogue.chat_id(), "Newsletter sent.").disable_notification(true).await?;
    dialogue.exit().await?;
    Ok(())

    // // Text message only
    // match msg.text().map(ToOwned::to_owned)
    // {
    //     Some(text) =>
    //     {
    //         for id in database::RegisteredUsers::get_subscribers()
    //         {
    //             bot.send_message(ChatId(id), text).await?;
    //             dialogue.exit().await?;
    //         }
    //     }
    //     None =>
    //     {
    //         bot.send_message(msg.chat.id, "Пожалуйста укажите вашу заметку.").disable_notification(true).await?;
    //     }
    // }
    // Ok(())
}

async fn subscription_menu(
    bot: Bot,
    dialogue: MyDialogue,
    q: CallbackQuery,
    is_subscribed: bool,
) -> HandlerResult
{
    if let Some(inline_button) = &q.data
    {
        match inline_button.as_str()
        {
            BACK_BUTTON => 
            {
                bot.edit_message_text(dialogue.chat_id(), q.message.unwrap().id, Command::descriptions().to_string()).await?;
                dialogue.exit().await?;
            }
            _ =>
            {
                if let Ok(_) = database::RegisteredUsers::add_modify_user(dialogue.chat_id().0, database::ModifyUser::Subscription)
                {
                    let text = if is_subscribed { "Unsubed." } else { "Subed." };
                    bot.edit_message_text(dialogue.chat_id(), q.message.unwrap().id, text).await?;
                    dialogue.exit().await?;
                }
            },
        }
    }
    Ok(())
}


async fn cancel(bot: Bot, dialogue: MyDialogue, msg: Message) -> HandlerResult {
    bot.send_message(msg.chat.id, "Cancelling the dialogue.").await?;
    dialogue.update(State::default()).await?;
    dialogue.exit().await?;
    Ok(())
}

async fn invalid_state(bot: Bot, msg: Message) -> HandlerResult {
    bot.send_message(msg.chat.id, "Unable to handle the message. Type /help to see the usage, or cancel an order with /cancel").await?;
    Ok(())
}

//
// Helper functions
//

// Change state to final step before sending details
async fn finalize_markup(
    bot: Bot,
    dialogue: MyDialogue,
    buyer: BuyerInfo
) -> HandlerResult
{
    let keyboard = create_menu_keyboard(MenuMarkup::Finalize);
    let text = format!("Спасибо. Ваш заказ:\n {:#?}", buyer.order);
    let media = InputFile::file(format!("{}/{}", env!("CARGO_MANIFEST_DIR"), LOGO_PHOTO));

    bot.send_photo(dialogue.chat_id(), media).caption(text).reply_markup(keyboard).await?;
    dialogue.update(State::Final(buyer)).await?;
    
    Ok(())
}

// Chnage state to main menu
async fn switch_to_main(
    bot: Bot,
    dialogue: MyDialogue,
    message_id: MessageId,
    buyer: BuyerInfo
) -> HandlerResult
{
    let keyboard = create_menu_keyboard(MenuMarkup::Main);
    let pic = InputFile::file(format!("{}/{}", env!("CARGO_MANIFEST_DIR"), LOGO_PHOTO));
    let pic = InputMediaPhoto::new(pic).caption(MENU_SEPARATOR);
    let media = InputMedia::Photo(pic);

    bot.edit_message_media(dialogue.chat_id(), message_id, media).reply_markup(keyboard).await?;
    dialogue.update(State::Main(buyer)).await?;

    Ok(())
}

fn create_menu_keyboard(state: MenuMarkup) -> InlineKeyboardMarkup
{
    let mut keyboard: Vec<Vec<InlineKeyboardButton>> = vec![];
    let mut submenus: Vec<&str> = vec![];

    // Button layout
    match state
    {
        MenuMarkup::Main => MENU.iter().for_each(|x| submenus.push(x.name)),
        MenuMarkup::Submenu(kind) => CONFIG[kind].iter().for_each(|x| submenus.push(x)),
        MenuMarkup::Cart => submenus = [ORDER_BUTTON].to_vec(),
        MenuMarkup::Finalize => submenus = [NOTE_BUTTON, SEND_BUTTON].to_vec(),
        MenuMarkup::Subscription(is_subscribed) => submenus = if is_subscribed { [UNSUBSCRIBE_BUTTON].to_vec() } else { [SUBSCRIBE_BUTTON].to_vec() }
    }

    // Push buttons into array of arrays for an inline keyboard
    // .chunks(1) means one item in a row
    for versions in submenus.chunks(2)
    {
        let row = versions
            .iter()
            .map(|version| InlineKeyboardButton::callback(version.to_owned(), version.to_owned()))
            .collect();

        keyboard.push(row);
    }

    // Checkout button if in main menu, otherwise show a button to return to a main menu
    if let MenuMarkup::Main = state
    {
        let cart_button = [InlineKeyboardButton::callback(CART_BUTTON, CART_BUTTON)];
        keyboard.push(cart_button.to_vec());
    }
    else
    {
        let back_button = [InlineKeyboardButton::callback(BACK_BUTTON, BACK_BUTTON)];
        keyboard.push(back_button.to_vec());
    }

    InlineKeyboardMarkup::new(keyboard)
}


impl fmt::Display for State
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        fmt::Debug::fmt(self, f)
    }
}
