use std::fs::OpenOptions;
use std::collections::HashMap;
use teloxide::types::Location;
use ron::de::from_reader;
use ron::ser::{to_string_pretty, PrettyConfig};
use serde::{Deserialize, Serialize};

#[derive(Default, Deserialize, Serialize)]
struct User
{
    name: Option<String>,
    location: Option<Location>,
    total_orders_sum: f32,
    is_subscribed: bool,
}

pub enum ModifyUser
{
    Nothing,
    Subscription,
    Name(String),
    Location(Location),
    TotalOrders(f32),
}

#[derive(Deserialize, Serialize)]
pub struct RegisteredUsers
{
    users: HashMap<i64, User>
}

impl RegisteredUsers
{
    pub fn add_modify_user(id: i64, modify: ModifyUser) -> Result<(), std::io::Error>
    {
        let users_file_path = format!("{}/users.ron", env!("CARGO_MANIFEST_DIR"));

        // Need read permission for matching a file.
        // Write permisson for creating a file if it doesnt exist for some reason
        let file = OpenOptions::new().read(true).write(true).create(true).open(users_file_path.clone())?;

        // Create a new hash map from file if it's valid, otherwise make an empty HashMap
        let mut registered_users: Self = match from_reader(file) {
            Ok(x) => x,
            Err(e) => {
                // If a file already exists with some data inside - Copy that file into a new .bak file
                // This will happen if users.ron was edited manually and formatting got messed up
                let len = std::fs::metadata(users_file_path.clone())?;
                if len.len() > 0
                {
                    log::warn!("File isn't formated properly. Creating a new file and making a backup of an old one. {:?}", e);
                    std::fs::copy(users_file_path.clone(), format!("{}/users.ron.bak", env!("CARGO_MANIFEST_DIR")))?;
                }

                let mut users = HashMap::new();
                users.insert(id, User::default());
                Self { users }
            }
        };

        // This line is the reason why it's called add_and_modify
        // If a user dosn't exist yet - add him to the list with default values (subscribed)
        // If it does - check what needs to be updated, and set new values.
        registered_users.users.entry(id).and_modify(|e| {
            match modify
            {
                ModifyUser::Subscription => e.is_subscribed = !e.is_subscribed,
                ModifyUser::Name(name) => e.name = Some(name),
                ModifyUser::Location(loc) => e.location = Some(loc),
                ModifyUser::TotalOrders(amount) => e.total_orders_sum += amount,
                _ => (),
            }
        }).or_insert(User::default());

        // Prepare a serialized string with indentation to write to a file
        let pretty = PrettyConfig::new().depth_limit(3);
        let s = to_string_pretty(&registered_users, pretty).expect("Serialization failed");
        std::fs::write(users_file_path, s)
    }

    pub fn check_subscription(id: i64) -> bool
    {
        let users_file_path = format!("{}/users.ron", env!("CARGO_MANIFEST_DIR"));
        let file = OpenOptions::new().read(true).open(users_file_path).expect("Couldn't read a file users.ron. Run /start");
        let map: Self = from_reader(file).expect("Couldnt deserialize a file");

        // If user is already registered - return his subscription status
        // Otherwise he isn't registered yet and obviously isn't subscribed
        if let Some(usr) = map.users.get(&id)
        {
            return usr.is_subscribed;
        }
        false
    }

    pub fn get_subscribers() -> Vec<i64>
    {
        let users_file_path = format!("{}/users.ron", env!("CARGO_MANIFEST_DIR"));
        let file = OpenOptions::new().read(true).open(users_file_path).expect("Couldn't read a file users.ron. Run /start");
        let map: Self = from_reader(file).expect("Couldmt parse a file");

        map.users.iter().filter_map(|(key, val)| val.is_subscribed.then_some(*key)).collect()
    }

    pub fn has_delivery_details(id: i64) -> Option<(Location, String)>
    {
        let users_file_path = format!("{}/users.ron", env!("CARGO_MANIFEST_DIR"));
        let file = OpenOptions::new().read(true).open(users_file_path).expect("Couldn't read a file users.ron. Run /start");
        let map: Self = from_reader(file).expect("Couldnt deserialize a file");
        let user = map.users.get(&id).expect("Couldnt get a user from users.ron while calling `has_delivery_details()`.");

        // If one of the Option<T> is None, then zip() will return None.
        user.location.zip(user.name.to_owned())
    }
}
