// To add/remove an item to a submeny, or to add a new submenu to the menu - follow the
// instructions.

use std::ops::Index;

#[derive(Debug)]
pub struct MenuInfo<T: AsRef<str>>
{
    pub name: T,
    pub kind: MenuKind,
    pub description: T,
    pub pic: T,
}

// 1. If you need to add or remove a submenu, first add a new entry here (or remoove it)
#[derive(Debug)]
pub struct Config<'a>
{
    sets: &'a [&'static str],
    sushi: &'a [&'static str],
    new: &'a [&'static str],
    discount: &'a [&'static str],
    home: &'a [&'static str],
    dairy: &'a [&'static str],
    meat: &'a [&'static str],
    salad: &'a [&'static str],
}

// 2. Also add it here. Names don't need to be the same, but keeps things clear, name it similarly
#[derive(Clone, Copy, Debug)]
pub enum MenuKind
{
    Sets,
    Sushi,
    New,
    Discount,
    Home,
    Dairy,
    Meat,
    Salad,
}

// pub const ADMIN_ID: u64 = "UserId of admin";

pub const MENU_SEPARATOR: &str = "----------~~~~~~~~====Меню====~~~~~~~~----------";
pub const NEWSLETTER: &str = "Использьівая єтот бот вьі автоматически продписьіваетесь на розсьілку с нашими последними предложениями.";
pub const BACK_BUTTON: &str = "⬅ Назад";
pub const CART_BUTTON: &str = "🛒 Кошик";
pub const SUBSCRIBE_BUTTON: &str = "Підписатися";
pub const UNSUBSCRIBE_BUTTON: &str = "Відписатися";
pub const KEEP_PREVIOUS_INFO: &str = "Залищити";
pub const CHANGE_PREVIOUS_INFO: &str = "Змінити";
pub const EMPTY_CART_TEXT: &str = "Ваш кошик поки що пустий";
pub const ORDER_BUTTON: &str = "Замовити";
pub const SEND_BUTTON: &str = "Надіслати";
pub const NOTE_BUTTON: &str = "Залишити примітку";

pub const LOGO_PHOTO: &str = "pics/logo2.jpg";
pub const CART_PHOTO: &str = "pics/ohurci.jpg";

// 3. If you only need to edit an existent submenu - change the fields here.
// To add a new item to a submenu - add a new ProductInfo { ... } to a vector.
// Keep track of the punctuation
// For a new submenu - write a it as in Config struct <submenu>: &[ProductInfo {...}, ...]
pub const CONFIG: Config = Config
{
    sets: &[ "Преміум: 59€", "Фантастік: 41€", "Гурман: 32€", "Веганський: 20€" ],
    sushi: &[ "Чука (2шт): 4€", "Сяке суши (2шт): 5.5€", "Унагі суші (2шт): 5.50€", "Ебі суші (2шт): 5.5€", 
                "Спайс сяке (2шт): 6.0€", "Спайс унагі (2шт): 6.0€", "Спайс ебі (2шт): 6.0€", "Суші попс (2шт): 4.0€" ],
    new: &[ "Пшено з лососем", "Суші по селяньски", "Салат 5-ий сезон" ],
    discount: &[ "Каховський борщ", "Суші сексі 4шт", "Млинці із млинцями" ],
    home: &[ "Млинці 1кг", "Вареники 1кг", "Пельмені 1кг", "Сирники 1кг", "Курячий паштет 200г", "Пельмені ґедзя 1кг", "Бульйон 1л" ],
    dairy: &[ "Масло топлене 300г", "Сметана 500г", "Сир 1кг", "Масло домашнє 200г", "Вершки 200г" ],
    meat: &[ "Фарширований перець 1кг", "Голубці 1кг", "Котлети 1кг", "Фрікадельки 1 кг", "Тефтелі 1 кг" ],
    salad: &[ "Олів'є 1кг", "Вінегрет 1кг", "Крабовий 1кг", "Мімоза 1кг", "Шуба 1кг", "Квашена капуста 1кг",
                "Риба хє 1кг", "Баклажани по-корейськи 1кг", "Морква по-корейськи 1кг", "Кабачок по-корейськи 1кг",
                "Шампіньйони по-корейськи 1кг", "Капуста по-корейськи 1кг" ],
};

// 4. If you created a new submenu, add it here
// Make sure to change the number in definition to match the number of elements insdide
pub const MENU: &[MenuInfo<&'static str>; 8] =
&[
    MenuInfo {
        name: "🍱 Японські сети",
        kind: MenuKind::Sets,
        description: "Опис меню. Якщо окремий текст для цього не потрібен, то видалити MenuInfo.description. та просто написати кнопку",
        pic: "pics/menu_sets.jpg" },
    MenuInfo {
        name: "🍣 Суші",
        kind: MenuKind::Sushi,
        description: "Опис меню. Якщо окремий текст для цього не потрібен, то видалити MenuInfo.description. та просто написати кнопку",
        pic: "pics/menu_sushi.jpg" },
    MenuInfo {
        name: "🔥 Новинки",
        kind: MenuKind::New,
        description: "Опис меню. Якщо окремий текст для цього не потрібен, то видалити MenuInfo.description. та просто написати кнопку",
        pic: "pics/syr.jpg" },
    MenuInfo {
        name: "❗ Акційні пропозиції",
        kind: MenuKind::Discount,
        description: "Опис меню. Якщо окремий текст для цього не потрібен, то видалити MenuInfo.description. та просто написати кнопку",
        pic: "pics/varenyky.jpg" },
    MenuInfo {
        name: "🥞 Ручна ліпка",
        kind: MenuKind::Home,
        description: "Опис меню. Якщо окремий текст для цього не потрібен, то видалити MenuInfo.description. та просто написати кнопку",
        pic: "pics/menu.jpg" },
    MenuInfo {
        name: "🥛 Молочні вироби",
        kind: MenuKind::Dairy,
        description: "Опис меню. Якщо окремий текст для цього не потрібен, то видалити MenuInfo.description. та просто написати кнопку",
        pic: "pics/menu_dairy.jpg" },
    MenuInfo {
        name: "🍗 М'ясні вироби",
        kind: MenuKind::Meat,
        description: "Опис меню. Якщо окремий текст для цього не потрібен, то видалити MenuInfo.description. та просто написати кнопку",
        pic: "pics/menu_meat.jpg" },
    MenuInfo {
        name: "🥗 Салати",
        kind: MenuKind::Salad,
        description: "Опис меню. Якщо окремий текст для цього не потрібен, то видалити MenuInfo.description. та просто написати кнопку",
        pic: "pics/menu_salads.jpg"}
];

// 5. Either remove or add a new entry for a match pattern
// MenuKind::<MenuKind field> => &self.<Config field>,
impl Index<MenuKind> for Config<'_>
{
    type Output = [&'static str];

    fn index(&self, index: MenuKind) -> &Self::Output
    {
        match index
        {
            MenuKind::Sets => &self.sets,
            MenuKind::Sushi => &self.sushi,
            MenuKind::New => &self.new,
            MenuKind::Discount => &self.discount,
            MenuKind::Home => &self.home,
            MenuKind::Dairy => &self.dairy,
            MenuKind::Meat => &self.meat,
            MenuKind::Salad => &self.salad,
        }
    }
}
